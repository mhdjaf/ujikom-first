<?php

namespace App\Http\Controllers;

use App\Models\jurnal;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JurnalController extends Controller
{
    public function index()
    {
        $data = User::all()->where('id', Auth()->User()->id);
        return view('Jurnal.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('jurnals')->insert([
            'user_id' => $request->user_id,
            'tanggal' => $request->tanggal,
            'kegiatan' => $request->kegiatan,
            'jam_masuk' => $request->jam_masuk,
            'jam_keluar' => $request->jam_keluar,
            'created_at' => now()
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = jurnal::all()->where('id', $id);
        return view('Jurnal.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('jurnals')->where('id', $request->id)->update([
            'tanggal' => $request->tanggal,
            'kegiatan' => $request->kegiatan,
            'keterangan' => $request->keterangan,
            'jam_masuk' => $request->jam_masuk,
            'jam_keluar' => $request->jam_keluar
        ]);
        return redirect('/jurnal');
    }
    public function delete($id)
    {
        DB::table('jurnals')->where('id', $id)->delete();
        return redirect()->back();
    }
}
