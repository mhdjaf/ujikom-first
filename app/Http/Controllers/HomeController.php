<?php

namespace App\Http\Controllers;

use App\Models\jurusan;
use App\Models\perusahaan;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\pembimbing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function kodeakses(Request $request)
    {
        if ($request->kodeakses == "alfalahjuara") {
            return Redirect('/register');
        } else {
            return redirect()->back()->withErrors("Kode akses Salah!")->withInput();
        }
    }
    public function login4admin()
    {
        return view('Auth.login4admin');
    }
    public function index()
    {
        if (Auth()->user()->level == 'siswa') {
            # code...
            return redirect('/dashboard/siswa');
        } elseif (Auth()->user()->level == 'pembimbing') {
            return redirect('/dashboard/pembimbing');
        }
        return redirect('/adminHome');
    }
    public function ShowTables()
    {
        $siswa = User::paginate(10)->where('level', "siswa")->sortBy('created_at');
        $pembimbing = Pembimbing::paginate(5)->sortBy('name');
        $perusahaan = perusahaan::paginate(5)->sortBy('name');
        $jurusan = jurusan::all()->sortBy('name');
        return view(
            'ShowAllTables',
            [
                'siswa' => $siswa,
                'pembimbing' => $pembimbing,
                'perusahaan' => $perusahaan,
                'jurusan' => $jurusan
            ]
        );
    }
    public function table_siswa()
    {
        $siswa = User::all()->where('level', "siswa")->sortBy('nis');
        return view('Siswa.siswaTables', ['data' => $siswa]);
    }
    public function siswa_search(Request $request)
    {
        $cari = $request->cari;
        $siswa = DB::table('users')
            ->where('name', 'like', '%' . $cari . '%')
            ->orwhere('nis', 'like', '%' . $cari . '%');
        $siswa = $siswa->get();
        return view('Siswa.siswaTables', ['data' => $siswa]);
    }
    public function table_pembimbing()
    {
        $pembimbing = pembimbing::all()->sortBy('name');
        return view('Pembimbing.pembimbingTable', ['data' => $pembimbing]);
    }
    public function table_perusahaan()
    {
        $perusahaan = perusahaan::all()->sortBy('name');
        $jurusan = jurusan::all()->sortBy('name');
        return view('Pembimbing.perusahaanTable', [
            'data' => $perusahaan,
            'jurusan' => $jurusan
        ]);
    }
    public function profile()
    {
        return view('profile');
    }
}
