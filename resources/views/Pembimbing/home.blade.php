@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container">
    <div class="row mt-4">
        <div class="col-lg-12">
            <table class="table table-striped table-responsive text-center text-primary">
                <thead>
                    <tr>
                        <th class="text-center">NIS</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Nama Pembimbing</th>
                        <th class="text-center">Progress</th>
                        <th class="text-center">Penyusunan Laporan</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody class="text-dark">
                    @foreach ($data as $item)
                    <tr>
                        <td scope="row">{{ $item->nis }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                            @if ($item->pembimbing_id == null)
                            Siswa Belum Memiliki Pembimbing
                            @else
                            {{ $item->pembimbing->name }}
                            @endif
                        </td>
                        <td>
                            @if ($item->status == null)
                            Siswa belum memulai progress
                            @else
                            {{ $item->status }}

                            @endif
                        </td>
                        <td>
                            @if ($item->laporan == null)
                            Siswa belum menyusun Laporan
                            @else
                                <a href="/download/{{ $item->laporan->file_laporan }}" class="btn btn-outline-primary btn-sm">Download Laporan</a>
                            @endif
                        </td>
                        <td>
                            <a href="/detail/{{ $item->id }}" class="btn btn-primary btn-sm">Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="/home" class="btn btn-primary btn-sm mb-5">Kembali</a>
        </div>
    </div>


</div>
@endsection