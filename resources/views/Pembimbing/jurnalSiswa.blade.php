@extends('layouts.pembimbing.dashboard')

@section('body')

    <div class="container mt-5">
        <div class="row ">
            <div class="col-lg-6">
                <table class="table table-striped table-responsive text-center">
                    <thead class="thead-inverse">
                        <tr >
                            <th class="text-center">NIS</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td scope="row">{{ $item->nis }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <a href="/dashboard/Cek_kegiatan/{{ $item->id }}" class="btn btn-primary bi bi-info"> Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr class="text-start">
                            <td>
                                <a href="/home" class="btn btn-danger bi bi-arrow-left"></a>    
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-lg-6">
                <img src="/img/logo.png" id="logo-admin" class="d-flex m-auto" alt="" style="width: 400px; height: 400px; opacity: 30%">
            </div>
        </div>
    </div>
@endsection