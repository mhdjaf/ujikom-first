@extends('layouts.pembimbing.dashboard')

@section('body')

<div class="container my-2">
    <table class="table table-responsive text-center">
        <thead>
            <tr>
                <th rowspan="2">Tanggal Prakerin</th>
                <th>Kegiatan</th>
                <th>Jam Kegiatan Prakerin</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $jurnal)
            <tr>
                <td>{{ $jurnal->tanggal }}</td>
                <td>{{ $jurnal->kegiatan }}</td>
                <td>{{ $jurnal->jam_masuk }} - {{ $jurnal->jam_keluar }}</td>
                <td>
                    @if ($jurnal->keterangan == 'Sudah DI cek Oleh Pembimbing')
                        <span class="btn btn-success">Sudah di cek oleh pembimbing</span>
                    @endif
                </td>
                <td>
                    @if ($jurnal->keterangan == null)
                    <form action="/updateKeteranganJurnal" method="POST">
                        @csrf
                        <input type="hidden" name="keterangan" value="Sudah DI cek Oleh Pembimbing">
                        <input type="hidden" name="id" value="{{ $jurnal->id }}">
                        <input type="submit" value="Saya Sudah Cek" class="btn btn-primary">
                    </form>
                    @elseif ($jurnal->keterangan == 'Sudah DI cek Oleh Pembimbing')
                        <button class="btn btn-primary" disabled>Saya Sudah Cek</button>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/dashboard/jurnalSiswa" class="btn btn-danger bi bi-arrow-left"></a>
</div>

@endsection