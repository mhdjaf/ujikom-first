@extends('layouts.siswa.dashboard')

@section('body')
<div class="container my-2">
    <div class="card">
        <div class="card-body">
        <div class="row">
        <div class="col-lg-2">
            <img src="\foto_users\{{ Auth()->User()->foto_users }}" alt="" width="150px" class="rounded-rectangle img-thumbnail">
        </div>
        <div class="col-lg-8 px-4 py-4">
            
            @if (Auth()->User()->level !='admin')
            <h3>{{ Auth()->User()->name }}</h3>
            <p>
                @if (Auth()->User()->level =='pembimbing')
                    NIP : {{ Auth()->User()->nis }}<br>
                    Jenis Kelamin : {{ Auth()->User()->jk }}<br>
                    Kejuruan : {{ Auth()->User()->jurusan->jurusan }}
                @else
                    NIS : {{ Auth()->User()->nis }}<br>
                    Kelas dan Jurusan : {{ Auth()->User()->kelas }} {{ Auth()->User()->jurusan->jurusan }} <br>
                    @if (Auth()->User()->pembimbing_id == !null)
                    Nama Pembimbing : {{ Auth()->User()->pembimbing->name }} <br>
                    @else
                    Nama Pembimbing : Belum memiliki pembimbing<br>
                    Progres : {{ Auth()->User()->status }}<br>
                    @endif
                    @if (Auth()->User()->perusahaan_id == !null)
                        Tempat PKL : {{ Auth()->User()->perusahaan->nama_perusahaan }} <br>
                    @else
                        Tempat PKL : Belum memilih tempat PKL<br>
                    @endif
                @endif
            </p>
            @endif
            <h5>User Information</h5>
            <p>
                Username : {{ Auth()->User()->username }}<br>
                Email : {{ Auth()->User()->email }}<br>
                No Telepon : {{ Auth()->User()->no_telp }}<br>
                <a href="/gantiPassword">Reset Password</a>
            </p>
        </div>
    </div>
        </div>
    </div>
</div>
<a href="/editProfile" class="btn btn-warning text-white">Edit</a>
<a href="/home" class="btn btn-danger">Kembali</a>


@endsection