@extends('layouts.siswa.dashboard')

@section('body')

<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col">
            <div class="row ">
                <div class="col d-flex">
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#tambah" data-whatever="@mdo" style="margin-bottom:15px; width:170px"><span class="fas fa-plus fa-sm"></span> Tambah kegiatan</button>
                </div>
            </div>

            <table class="table table-bordered">
                <thead class="text-center bg-primary text-white">
                    <tr>
                        <th rowspan="2">Tanggal</th>
                        <th>Kegiatan</th>
                        <th>Jam Kegiatan</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        @foreach ($item->jurnal as $jurnal)
                        <tr class="text-center">
                                <td>{{ $jurnal->tanggal }}</td>
                                <td>{{ $jurnal->kegiatan }}</td>
                                <td>{{ $jurnal->jam_masuk }} - {{ $jurnal->jam_keluar }}</td>
                                <td>
                                @if ($jurnal->keterangan == null)
                                
                                @else
                                    <button type="button" class="btn btn-success">Sudah Di cek Pembimbing sekolah</button>
                                @endif
                            
                            </td>
                        <td>
                            <a href="/jurnal/edit/{{ $jurnal->id }}" type="button" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                            <a href="/jurnal/delete/{{ $jurnal->id }}" type="button" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>

            <a href="/home" class="btn btn-danger bi bi-arrow-left btn-sm"></a>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Form tambah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/jurnal/tambah" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Tanggal :</label>
                            <input type="date" class="form-control" id="recipient-name" name="tanggal">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Kegiatan:</label>
                            <textarea class="form-control" id="message-text" name="kegiatan"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Jam Masuk:</label>
                            <input type="time" class="form-control" id="recipient-name" name="jam_masuk">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Jam keluar:</label>
                            <input type="time" class="form-control" id="recipient-name" name="jam_keluar">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection