@extends('layouts.siswa.dashboard')

@section('body')

<div class="container mt-4">
  <div class="row justify-content-center">
    <div class="col-md-8 mb-5">
      <form action="/editProfile" method="post">
        @csrf
        <div class="card">
          <div class="card-body">
            <h3>Profile</h3>
            <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
            <div class="mb-3">
              <label for="" class="form-label">Nama Lengkap</label>
              <input type="text" class="form-control" name="name" id="" value="{{ Auth()->user()->name }}">
            </div>
            @if (Auth()->User()->level == 'pembimbing')
               <div class="mb-3">
              <label for="" class="form-label">NIP</label>
              <input type="text" class="form-control" name="nis" id="" value="{{ Auth()->user()->nis }}" readonly>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Jurusan</label>
              <select class="form-control" name="jk" id="" style="height: 40px" readonly>
                <option selected value="{{ Auth()->User()->jk }}">{{ Auth()->User()->jk}}</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Jenis Kelamin</label>
              <select class="form-control" name="jk" id="" style="height: 40px" readonly>
                <option selected value="{{ Auth()->User()->jurusan_id }}">{{ Auth()->User()->jurusan->jurusan}}</option>
              </select>
            </div>
            @else
                
            <div class="mb-3">
              <label for="" class="form-label">NIS</label>
              <input type="text" class="form-control" name="nis" id="" value="{{ Auth()->user()->nis }}" readonly>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Kelas</label>
              <select class="form-control" name="kelas" id="" style="height: 40px" readonly>
                <option selected value="{{ Auth()->User()->kelas }}">{{ Auth()->User()->kelas }}</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Jurusan</label>
              <select class="form-control" name="jk" id="" style="height: 40px" readonly>
                <option selected value="{{ Auth()->User()->jk }}">{{ Auth()->User()->jk}}</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Jurusan</label>
              <select class="form-control" name="jurusan" id="" style="height: 40px" readonly>
                <option selected value="{{ Auth()->User()->jurusan_id }}">{{ Auth()->User()->jurusan->jurusan}}</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Pembimbing</label>
              @if (Auth()->User()->pembimbing_id == null)
              <input type="text" class="form-control" name="pembimbing" id="" value="Belum Memiliki Pembimbing" readonly>
              @else
              <input type="text" class="form-control" name="pembimbing" id="" value="{{ Auth()->user()->pembimbing->name }}" readonly>
              @endif
            </div>
            @endif
            <label for="" class="form-label">Foto</label>
            <div class="">
              <img width="150px" src="\foto_users\{{ Auth()->User()->foto_users }}">
            </div>

          </div>
        </div>
        <div class="card my-2">
          <div class="card-body">
            <h3>User Information</h3>
            <div class="mb-3">
              <label for="" class="form-label">Username</label>
              <input type="text" class="form-control" name="username" value="{{ Auth()->User()->username }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">email</label>
              <input type="email" class="form-control" name="email" value="{{ Auth()->User()->email }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">No Telepon</label>
              <input type="number" class="form-control" name="no_telp" value="{{ Auth()->User()->no_telp }}">
            </div>
            <div class="mb-3">
              <a href="/gantiPassword" class="btn btn-warning btn-sm text-white">Ganti Password</a>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-success btn-sm bi bi-upload"> Submit</button>
        <a href="/home" class="btn btn-danger btn-sm bi bi-arrow-left"> Kembali</a>
      </form>
    </div>
  </div>


</div>

@endsection