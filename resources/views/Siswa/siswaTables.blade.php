@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container">
    <div class="card mt-4">
        <div class="card-body">
            <form method="GET" action="/table/siswa/cari">
                <label for="">Cari Siswa : </label>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="input-group mb-3">
                            <input type="text"  name="cari" class="form-control" value="{{ old('cari') }}"  placeholder="Tulis Nama/NIS" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <button class="btn btn-outline-primary ml-2"type="submit" value="cari" id="button-addon2"><i class="bi bi-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-inverse table-inverse table-responsive text-center">
                <thead class="thead-inverse|thead-default">
                    <tr>
                        <th class="text-center">NIS</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Nama Pembimbing</th>
                        <th class="text-center">Progress</th>
                        <th class="text-center">aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    @if ($item->level == 'siswa')   
                    <tr>
                        <td scope="row">{{ $item->nis }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                            @if ( $item->pembimbing_id == null)
                            Belum memiliki pembimbing
                            @else

                            {{ $item->pembimbing->name == null }}
                            @endif
                        </td>
                        <td>
                            @if ($item->status == null)
                            Siswa belum memulai progress
                            @else
                            {{ $item->status }}

                            @endif
                        </td>
                        <td>
                            @if (Auth()->User()->level == 'pembimbing')

                            @if ( $item->status == 'Menunggu Proses Validasi Surat Pengantar')
                            <a href="/validasiPengantar/{{ $item->id }}" class="btn btn-info">Validasi Surat Pengantar</a>
                            @elseif ($item->status == 'Lembar Pengesahan di terima')
                            <button type="button" class="btn btn-primary">Lembar pengesahan Di terima</button>
                            @elseif ($item->status == 'Masa Sanggah balasan surat Pengantar/pegajuan')
                            <a href="/konfirmasiPengajuan/{{ $item->id }}" class="btn btn-info">Konfirmasi Surat Pengantar</a>
                            @elseif ($item->status == null)
                            <button type="button" class="btn btn-danger">Siswa belum memulai progress</button>
                            @endif
                            @endif
                            <a href="/detail/{{ $item->id }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                            @if (Auth()->User()->level == 'admin')
                                <a href="/editSiswa/{{ $item->id }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/deleteSiswa/{{ $item->id }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                            @endif
                        </td>
                    </tr>
                    @else    
                    @endif
                    @endforeach
                </tbody>
            </table>
            <a href="/table" class="btn btn-danger"><i class="bi bi-arrow-left"></i> Back</a>

        </div>
    </div>
</div>
@endsection