@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container mb-5">
    <div class="card mt-4">
        <div class="card-body">
        <div class="row">
        <div class="col-lg-2">
            <img src="\foto_users\{{ $data->foto_users }}" alt="" width="300px" class="rounded-rectangle" style="margin-right: 20px">
        </div>
        <div class="col-lg-8 px-4 py-4" style="margin-left:150px">
            <h3>{{ $data->name }}</h3>
            <p>
                Kelas dan Jurusan : {{ $data->kelas }}, {{ $data->jurusan->jurusan }} <br>
                Jenis Kelamin     : {{ $data->jk }} <br>
                @if ($data->perusahaan_id == !null)
                    Tempat PKL    : {{ $data->perusahaan->nama_perusahaan }} <br>
                @else
                    Tempat PKL    : Belum memilih tempat PKL<br>
                @endif
                Progres           : {{ $data->   status }}
                @if ($data->pengantar_pkl != null)
                    Surat Pengantar: <a href="/download/{{ $data->pengantar_pkl }}">Download Surat Pengantar</a>
                @endif
            </p>
            @if (Auth()->User()->level == 'admin')
                <h5>User Information</h5>
                <p>
                  Username: {{ $data->username }}<br>
                  Email: {{ $data->email }}<br>
                  Nomor Telepon: {{ $data->no_telp }}
                </p>
            @endif
        </div>
    </div>
  </div>
</div>
@if ($data->nilai_id == !null)   
<div class="card mt-3">
    <div class="card-body">
        <h3>Nilai</h3>
        <table class="table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Kehadiran</th>
                    <th>Tanggung Jawab</th>
                    <th>kedisiplinan</th>
                    <th>Pekerjaan</th>
                    <th>Tanggal dibuat</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td >{{ $data->nilai->kehadiran }}</td>
                        <td >{{ $data->nilai->tanggung_jawab }}</td>
                        <td >{{ $data->nilai->kedisiplinan }}</td>
                        <td >{{ $data->nilai->pekerjaan }}</td>
                        <td >{{ $data->nilai->created_at }}</td>
                    </tr>
                </tbody>
        </table>
        
    </div>
</div>
@endif
<div class="row">
    <div class="col mt-3">
        <a href="/dashboard/monitoring" class="btn btn-secondary bi bi-arrow-left"></a>
        @if (Auth()->User()->level == 'admin')
            <a href="/editSiswa/{{ $data->id }}" class="btn btn-success"><i class="fas fa-user-edit"></i></a>
            <a href="/deleteSiswa/{{ $data->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        @endif
    </div>
</div>

</div>
@endsection