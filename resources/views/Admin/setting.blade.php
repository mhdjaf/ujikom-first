@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="card mt-2">
    <div class="card-header bg-primary text-white">Notifikasi</div>
    <div class="card-body">
        <button type="button" class="btn btn-outline-primary btn-sm mb-2" data-toggle="modal" data-target="#tambah" data-whatever="@mdo"> Tambah Notifikasi</button>
        <table class="table table-striped table-responsive">
            <thead>
                <tr>
                    <th>Notifikasi</th>
                    <th>Created at</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data2 as $item)
            <tr>
                <td scope="row">{{ $item->kegiatan }}</td>
                <td>{{ $item->created_at }}</td>
                <td class="text-center">
                    <a href="/jurnal/delete/{{ $item->id }}" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus notifikasi ini?');"><span class="bi bi-trash"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>  
    </div>
</div>
<div class="card mt-4">
    <div class="card-header bg-primary text-white fw-bold">Tabel Keahlian</div>
    <div class="card-body">
        <button type="button" class="btn btn-outline-primary btn-sm mb-2" data-toggle="modal" data-target="#add" data-whatever="@mdo"><span class="bi bi-plus"></span> Jurusan</button>

        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New jurusan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/insertJurusan" method="POST">
                    @csrf
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama Keahlian</label>
                    <input type="text" class="form-control" id="recipient-name" name="jurusan">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Kepala Jurusan</label>
                    <input type="text" class="form-control" id="recipient-name" name="kepala_jurusan">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
            </div>
        </div>
        </div>
        <table class="table table-responsive">
            <thead class="text-center">
                <tr>
                    <th>ID</th>
                    <th>Nama Keahlian</th>
                    <th>Kepala Jurusan</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td scope="row">{{ $item->id }}</td>
                            <td>{{ $item->jurusan }}</td>
                            <td>{{ $item->kepala_jurusan }}</td>
                            <td>
                                <button type="button" class="btn btn-warning text-white btn-sm" data-toggle="modal" data-target="#edit" data-whatever="@mdo"><span class="bi bi-pencil"></span></button>
                                    {{-- Modal Edit --}}
                                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit jurusan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/updateJurusan" method="POST">
                                                @csrf 
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="col-form-label">Nama Keahlian</label>
                                                    <input type="text" class="form-control" id="recipient-name" name="jurusan" value="{{ $item->jurusan }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="col-form-label">Kepala Jurusan</label>
                                                    <input type="text" class="form-control" id="recipient-name" name="kepala_jurusan" value="{{ $item->kepala_jurusan }}">
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </form>
                                        </div>
                            </td>
                            <td>
                                <a href="/deleteJurusan/{{ $item->id }}" class="btn btn-danger btn-sm"><span class="bi bi-trash"></span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
        
    </div>
</div>
<div class="card my-4">
    <div class="card-header bg-primary text-white fw-bold">Template Laporan Prakerin</div>
    <div class="card-body">
        @if (Auth()->User()->laporan_id == null)
        <form action="/createLaporan" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="1">
            <input type="hidden" name="nis" value="1">
            <label for="">Tata cara pembuatan laporan :</label><br>
            <textarea name="keterangan" id="" cols="30" rows="10"></textarea>
            <div class="mb-3">
                <label for="" class="form-label">Upload Laporan Prakerin</label>
                <input type="file"
                class="form-control" name="file_laporan" id="" aria-describedby="helpId" placeholder="">
            </div>
            <input type="submit" value="Submit" class="btn btn-primary btn-sm">
        </form>
        @else
        @foreach ($data1 as $item)
        Template Laporan PRAKERIN saat ini : <a href="/download/{{ $item->file_laporan }}">Download Template laporan saat ini</a> <br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadLaporan" data-whatever="@mdo">Upload Laporan Baru</button>

                <div class="modal fade" id="uploadLaporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/updateLaporan" method="post" enctype="multipart/form-data">
                                    @csrf
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <input type="hidden" name="keterangan" value="template">
                                        <label for="">Tata cara pembuatan laporan</label><br>
                                        <textarea name="keterangan" id="" cols="30" rows="10"></textarea>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Upload Laporan:</label>
                                            <input type="file" class="form-control" id="recipient-name" name="file_laporan">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
        @endif
    </div>
</div>
<a href="/home" class="btn btn-danger btn-sm mb-5">Kembali</a>

{{-- Modal --}}
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form tambah notifikasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/jurnal/tambah" method="post">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
                    <input type="hidden" name="jam_masuk" value="00:00">
                    <input type="hidden" name="jam_keluar" value="00:00">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Notifikasi yang ditampilkan : </label>
                        <input type="text" class="form-control" id="recipient-name" name="kegiatan" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection