@extends('layouts.pembimbing.dashboard')

@section('body')

  <div class="row">

    <div class="col-lg-5 d-flex justify-content-center">
      <img src="/img/4.jpg" alt="" id="image" style="width: 470px">
    </div>

    <div class="col-lg-7 mt-4">
      
      <div class="ml-5 mt-3">
        <span class="text-uppercase" id="title" style="font-weight: 500">menu</span>
      </div>
      
      <div class="row" id="menu" style="margin-left: 33px; margin-top: 50px">
        <div class="col-xs-7" id="kegiatan">
          <a href="/dashboard/jurnalSiswa"><img src="/img/jurnal.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
          <p id="labelPilihJurusan" class="mb-3" style="margin-left: 17px; margin-top:-25px"><br>Cek kegiatan<br>Siswa</p>
        </div>
      
        <div class="col-xs-7 text-end" id="pengajuan">
          <a href="/dashboard/Pengajuan_PKL"><img src="/img/pengajuan.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
          <p id="labelPengajuanPKL" style="margin-left: 17px; margin-top:-25px"><br>Pengajuan<br>PKL</p>
        </div>

        <div class="col-xs-4" id="monitoring">
          <a href="/dashboard/monitoring"><img src="/img/monitoring.png" alt="jurnal" style="width: 60px; margin-right: 100px"></a>
          <p id="labelInformasi" style="margin-left: 7px; margin-top: -15px"><br>Monitoring</p>
        </div>

        <div class="col-xs-4" id="informasi">
          <a href="/table"><img src="/img/tabel.png" alt="jurnal" style="width: 70px; margin-right: 100px; margin-top: 15px"></a>
          <p id="labellaporan" style="margin-left: 17px; margin-top: -5px">Tabel</p>
        </div>

        <div class="col-xs-4" id="setting">
          <a href="/setting"><img src="/img/setting.png" alt="jurnal" style="width: 60px; margin-right: 100px; margin-top: 18px"></a>
          <p id="labellaporan" style="margin-left: 7px;">Setting</p>
        </div>
        
        <div class="col-xs-4" id="setting">
          <a type="button" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"> <img src="/img/buat.png" alt="" style="width: 60px; margin-left:10px; margin-right: 100px; margin-top: 15px"></a>
          <p id="labellaporan" style="margin-left: 17px;">Buat Akun</p>
        </div>
      </div>
    </div>

  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <form action="/createAccount" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card">
            <div class="card-body">
            <h3>Profile</h3>
                <div class="mb-3">
                <label for="" class="form-label">Nama Lengkap</label>
                <input type="text" class="form-control" name="name" id="" required >
                </div>
                <div class="mb-3">
                <label for="" class="form-label">NIS/NIP</label>
                <input type="text" class="form-control" name="nis" id="" required >
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Jenis Kelamin</label>
                  <select class="form-control" name="jk" id="">
                    <option value=" ">--Pilih Jenis Kelamin--</option>
                    <option>Laki laki</option>
                    <option>Perempuan</option>
                  </select>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Kelas</label>
                <select class="form-control" name="kelas" id="" style="height: 40px">
                    <option selected value="">--Pilih Kelas--</option>
                    <option value="X">X</option>
                    <option value="XI">XI</option>
                    <option value="XII">XII</option>
                </select>
                <small id="helpId" class="form-text text-muted">Khusus untuk akun siswa</small>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Jurusan</label>
                <select class="form-control" name="jurusan" id="" style="height: 40px" required>
                  <option selected value="">--Pilih Jurusan--</option>
                  @foreach ($jurusan as $jurusan)
                    <option value="{{ $jurusan->id }}">{{ $jurusan->jurusan }}</option>
                  @endforeach
                </select>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Foto</label>
                <input type="file" class="form-control" name="foto" id="" required>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Level</label>
                  <select class="form-control" name="level" id="" style="height: 40px" required>
                    <option value="siswa">Siswa</option>
                    <option value="pembimbing">Pembimbing</option>
                  </select>
                </div>
            </div>
            </div>
            <div class="card">
            <div class="card-body">
            <h3>User Information</h3>
                <div class="mb-3">
                  <label for="" class="form-label">Username</label>
                  <input type="text" class="form-control" name="username" required >
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">email</label>
                  <input type="email" class="form-control" name="email" required>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">No Telepon</label>
                  <input type="number" class="form-control" name="no_telp" placeholder="62xxxxxxx" required>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">password</label>
                  <input type="password" class="form-control" name="password" required>
                </div>
            </div>
            </div>
    
        </div>
          
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Buat Akun</button>
          </form>
        </div>
      </div>
    
    </div>
  </div>

@endsection