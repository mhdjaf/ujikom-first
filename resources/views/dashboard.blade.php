@extends('layouts.admin.dashboard')

@section('body')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Sistem Informasi PKL <span> (SIP)</span><br>SMK Al Falah
    </h1>
    <h1 class="display-4"> </h1>

  </div>
</div>

<div class="container">
  <!--info panel -->
  <div class="row justify-content-center">
    <div class="col-10 info-panel">
      <div class="row">
        <div class="col-lg">
          <a href="/login/admin" class="text-decoration-none">
            <img src="img/6.jpeg" alt="employee" class="float-left">
            <h4 class="text-dark">Admin</h4>
          </a>

        </div>
        <div class="col-lg">
          <a href="/login" class="text-decoration-none">
            <img src="img/7.jpeg" alt="employee" class="float-left">
            <h4 class="text-dark">Pembimbing</h4>
          </a>

        </div>
        <div class="col-lg">
          <a href="/login" class="text-decoration-none">
            <img src="img/5.jpeg" alt="employee" class="float-left">
            <h4 class="text-dark">siswa</h4>
          </a>

        </div>
      </div>
    </div>
  </div>
  <!-- akhir panel -->
  {{-- <section class="testimonial">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <h5>"Tidak peduli seberat apapun atau tidak mungkin untuk dicapai, kau tidak boleh menyerah dengan tujuanmu"</h5>
        <h5> -luffy one piece</h5>
      </div>
    </div> --}}
    <!-- akhirworkingspace -->
</div>

@endsection