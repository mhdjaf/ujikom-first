<nav class="navbar navbar-expand-lg navbar-light">
  <div class="container">
    <img src="/img/logolagi.png" alt="logosmk" class="rounded-circle" style="width: 50px; margin-right: 4px">
    <a class="navbar-brand" href="#">SMK AL-FALAH</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ml-auto">
        <a class="nav-link active" href="#">Home <span class="sr-only">(current)</span></a>

        <a class="nav-link" href="#">Contact</a>
        <a class="nav-link">About</a>

      </div>
    </div>
  </div>
</nav>